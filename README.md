# EKS Cluster Auto-scaling

## Summary

This POC demonstrates how a Kuberenetes cluster (EKS) scales using the various resources available
within the cluster.

This repo provides the following resources:
* A deployment with pods that use 1GB of memory and replicaset min 3 max 12 (apisvc-1g-2g.yaml)
* An HPA which controls the scaling of deployment above (hpa-apisvc.yaml)
* An over-provisioner deployment which requests 2100Mi of memory per pod - 2 replicas (overprovision.yaml)

The scale in and scale out of the deployment is controlled by the HPA. Be warned it is set to 500Mi
Therefore the deployment will scale very quickly to 12 pods. To adjust the scale in of the deployment edit 
the HPA metrics.resource.target from averageValue: 500Mi --> averageValue: 1200Mi (This is above the memory usage of
the apisvc pods)

You may need to add more pods to the over-provisioner deployment depending on the cluster resources being used / node 
allocatable resources etc. Alternatively you can scale the deployment out by adding additional replicas.

Play with it until you see an overprovisioner pod go into pending state which should then trigger cluster-autoscaler to add
an additional node.

The resources can be added to the cluster via kubectl. Something like this.

kubectl create -f apisvc-1g-2g.yaml -n <some-namespace>

